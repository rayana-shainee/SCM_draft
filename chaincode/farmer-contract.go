package main

import (
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	
	//"strconv"
	//"time"
)

// Define constants
const productPrefix = "PROD"
const eventPrefix = "EVENT"

// Define Product and Event structures
type Product struct {
	ProductID     string `json:"productID"`
	ProductName   string `json:"productName"`
	Quantity      int    `json:"quantity"`
	Cost          int    `json:"cost"`
	Farmer        string `json:"farmer"`
	IsOrganic     bool   `json:"isOrganic"`
}

type Event struct {
	ProductID     string `json:"productID"`
	Event		  string `json:"event"`
	EventDate     string `json:"eventDate"`
}

type ProductWithEvents struct {
	Product   Product     `json:"product"`
	Events	  []*Event 	  `json:"events"`
}

type SmartContract struct {
	contractapi.Contract
}

// AddProduct allows a farmer to add product details
func (s *SmartContract) AddProduct(ctx contractapi.TransactionContextInterface, args string) Response {
	product := &Product{}
	err := JSONtoObject([]byte(args), product)
	if err != nil {
		return BuildResponse("ERROR", fmt.Sprintf("Failed to parse product details: %v", err), nil)
	}

	productKey, err := ctx.GetStub().CreateCompositeKey(productPrefix, []string{product.ProductID})

	//check whether the product exists or not
	isProductExists, err := s.ProductExists(ctx, productKey)
	if isProductExists {
		return BuildResponse("DUPLICATE", fmt.Sprintf("Product already exists: %v", product.ProductID), nil)
	}
	if err != nil {
		return BuildResponse("ERROR", "Failed to check if product exists:", nil)
	}

	//converts the product details to JSON format
	productBytes, err := ObjecttoJSON(product)
	if err != nil {
		return BuildResponse("ERROR", "Failed to convert product to JSON", nil)
	}

	err = ctx.GetStub().PutState(productKey, productBytes)
	if err != nil {
		return BuildResponse("ERROR", "Failed to add product to the blockchain", nil)
	}
	return BuildResponse("SUCCESS", "Product added successfully", nil)
}

// AddEvent allows a farmer to add cultivation techniques and fertilizers used
func (s *SmartContract) AddEvent(ctx contractapi.TransactionContextInterface, args string) Response {
	event := &Event{}
	err := JSONtoObject([]byte(args), event)
	if err != nil {
		return BuildResponse("ERROR", fmt.Sprintf("Failed to parse event details: %v", err), nil)
	}

	eventKey, err := ctx.GetStub().CreateCompositeKey(eventPrefix, []string{event.ProductID, event.EventDate})

	eventBytes, err := ObjecttoJSON(event)
	if err != nil {
		return BuildResponse("ERROR", "Failed to convert event to JSON", nil)
	}

	err = ctx.GetStub().PutState(eventKey, eventBytes)
	if err != nil {
		return BuildResponse("ERROR", "Failed to add event to the blockchain", nil)
	}
	return BuildResponse("SUCCESS", "Event added successfully", nil)
}


// GetAllProductsWithEvents returns all products with their associated events and dates
func (s *SmartContract) GetAllProductsWithEvents(ctx contractapi.TransactionContextInterface) Response {
	// Get all products
	productResultsIterator, err := ctx.GetStub().GetStateByPartialCompositeKey(productPrefix, []string{})
	if err != nil {
		return BuildResponse("ERROR", "Failed to read product data from the blockchain", nil)
	}
	defer productResultsIterator.Close()

	var productsWithEvents []*ProductWithEvents

	for productResultsIterator.HasNext() {
		queryResponse, err := productResultsIterator.Next()
		if err != nil {
			return BuildResponse("ERROR", "Failed to read product data from the blockchain", nil)
		}

		var product Product
		err = json.Unmarshal(queryResponse.Value, &product)
		if err != nil {
			return BuildResponse("ERROR", "Failed to unmarshal product data", nil)
		}

		// Get associated events for the product
		eventResultsIterator, err := ctx.GetStub().GetStateByPartialCompositeKey(eventPrefix, []string{product.ProductID})
		if err != nil {
			return BuildResponse("ERROR", "Failed to read event data from the blockchain", nil)
		}
		defer eventResultsIterator.Close()

		var events []*Event
		for eventResultsIterator.HasNext() {
			eventQueryResponse, err := eventResultsIterator.Next()
			if err != nil {
				return BuildResponse("ERROR", "Failed to read event data from the blockchain", nil)
			}

			var event Event
			err = json.Unmarshal(eventQueryResponse.Value, &event)
			if err != nil {
				return BuildResponse("ERROR", "Failed to unmarshal event data", nil)
			}
			events = append(events, &event)
		}

		// Combine product and events
		productWithEvents := &ProductWithEvents{
			Product:   product,
			Events: events,
		}
		productsWithEvents = append(productsWithEvents, productWithEvents)
	}

	productsWithEventsBytes, err := ObjecttoJSON(productsWithEvents)
	if err != nil {
		return BuildResponse("ERROR", "Failed to marshal products with events data", nil)
	}

	return BuildResponse("SUCCESS", "", productsWithEventsBytes)
}



//read product with events
func (s *SmartContract) ReadProductWithEvents(ctx contractapi.TransactionContextInterface, productID string) Response {
    // Create the composite key for the product
    productKey, err := ctx.GetStub().CreateCompositeKey(productPrefix, []string{productID})
    if err != nil {
        return BuildResponse("ERROR", "Failed to create composite key for product", nil)
    }

    // Get the product from the state
    productBytes, err := ctx.GetStub().GetState(productKey)
    if err != nil {
        return BuildResponse("ERROR", "Failed to read product from the blockchain", nil)
    }
    if productBytes == nil {
        return BuildResponse("ERROR", fmt.Sprintf("Product with ID %s does not exist", productID), nil)
    }

    var product Product
    err = json.Unmarshal(productBytes, &product)
    if err != nil {
        return BuildResponse("ERROR", "Failed to unmarshal product data", nil)
    }

    // Query for all events associated with the product
    eventResultsIterator, err := ctx.GetStub().GetStateByPartialCompositeKey(eventPrefix, []string{productID})
    if err != nil {
        return BuildResponse("ERROR", "Failed to read events from the blockchain", nil)
    }
    defer eventResultsIterator.Close()

    var events []*Event
    for eventResultsIterator.HasNext() {
        queryResponse, err := eventResultsIterator.Next()
        if err != nil {
            return BuildResponse("ERROR", "Failed to read event data from the blockchain", nil)
        }

        var event Event
        err = json.Unmarshal(queryResponse.Value, &event)
        if err != nil {
            return BuildResponse("ERROR", "Failed to unmarshal event data", nil)
        }
        events = append(events, &event)
    }

    // Combine product details and events
    productWithEvents := &ProductWithEvents{
        Product: product,
        Events:  events,
    }

    productWithEventsBytes, err := ObjecttoJSON(productWithEvents)
    if err != nil {
        return BuildResponse("ERROR", "Failed to marshal product with events data", nil)
    }

    return BuildResponse("SUCCESS", "", productWithEventsBytes)
}


//delete product with events

func (s *SmartContract) DeleteProductWithEvents(ctx contractapi.TransactionContextInterface, productID string) Response {
    // Create the composite key for the product
    productKey, err := ctx.GetStub().CreateCompositeKey(productPrefix, []string{productID})
    if err != nil {
        return BuildResponse("ERROR", "Failed to create composite key for product", nil)
    }

    // Get the product from the state to check if it exists
    productBytes, err := ctx.GetStub().GetState(productKey)
    if err != nil {
        return BuildResponse("ERROR", "Failed to read product from the blockchain", nil)
    }
    if productBytes == nil {
        return BuildResponse("ERROR", fmt.Sprintf("Product with ID %s does not exist", productID), nil)
    }

    // Delete the product from the state
    err = ctx.GetStub().DelState(productKey)
    if err != nil {
        return BuildResponse("ERROR", "Failed to delete product from the blockchain", nil)
    }

    // Query for all events associated with the product
    eventResultsIterator, err := ctx.GetStub().GetStateByPartialCompositeKey(eventPrefix, []string{productID})
    if err != nil {
        return BuildResponse("ERROR", "Failed to read events from the blockchain", nil)
    }
    defer eventResultsIterator.Close()

    // Iterate over the events and delete each one
    for eventResultsIterator.HasNext() {
        queryResponse, err := eventResultsIterator.Next()
        if err != nil {
            return BuildResponse("ERROR", "Failed to read event data from the blockchain", nil)
        }

        // Delete the event from the state
        err = ctx.GetStub().DelState(queryResponse.Key)
        if err != nil {
            return BuildResponse("ERROR", "Failed to delete event from the blockchain", nil)
        }
    }

    return BuildResponse("SUCCESS", fmt.Sprintf("Product with ID %s and all associated events have been deleted successfully", productID), nil)
}


//update product
/*
func (s *SmartContract) UpdateProductDetailsWithEvents(ctx contractapi.TransactionContextInterface, productID string, productDetails string, eventsDetails string) Response {
    // Create the composite key for the product
    productKey, err := ctx.GetStub().CreateCompositeKey(productPrefix, []string{productID})
    if err != nil {
        return BuildResponse("ERROR", "Failed to create composite key for product", nil)
    }

    // Get the product from the state to check if it exists
    productBytes, err := ctx.GetStub().GetState(productKey)
    if err != nil {
        return BuildResponse("ERROR", "Failed to read product from the blockchain", nil)
    }
    if productBytes == nil {
        return BuildResponse("ERROR", fmt.Sprintf("Product with ID %s does not exist", productID), nil)
    }

    // Unmarshal the new product details
    var product Product
    err = json.Unmarshal([]byte(productDetails), &product)
    if err != nil {
        return BuildResponse("ERROR", "Failed to unmarshal new product data", nil)
    }

    // Marshal the updated product back to JSON
    updatedProductBytes, err := ObjecttoJSON(product)
    if err != nil {
        return BuildResponse("ERROR", "Failed to marshal updated product data", nil)
    }

    // Put the updated product back to the state
    err = ctx.GetStub().PutState(productKey, updatedProductBytes)
    if err != nil {
        return BuildResponse("ERROR", "Failed to update product in the blockchain", nil)
    }

    // Query for all existing events associated with the product
    eventResultsIterator, err := ctx.GetStub().GetStateByPartialCompositeKey(eventPrefix, []string{productID})
    if err != nil {
        return BuildResponse("ERROR", "Failed to read events from the blockchain", nil)
    }
    defer eventResultsIterator.Close()

    // Delete all existing events associated with the product
    for eventResultsIterator.HasNext() {
        queryResponse, err := eventResultsIterator.Next()
        if err != nil {
            return BuildResponse("ERROR", "Failed to read event data from the blockchain", nil)
        }

        // Delete the event from the state
        err = ctx.GetStub().DelState(queryResponse.Key)
        if err != nil {
            return BuildResponse("ERROR", "Failed to delete existing event from the blockchain", nil)
        }
    }

    // Unmarshal the new events details
    var events []Event
    err = json.Unmarshal([]byte(eventsDetails), &events)
    if err != nil {
        return BuildResponse("ERROR", "Failed to unmarshal new events data", nil)
    }

    // Put the updated events back to the state
    for _, event := range events {
        eventKey, err := ctx.GetStub().CreateCompositeKey(eventPrefix, []string{productID, event.EventID})
        if err != nil {
            return BuildResponse("ERROR", "Failed to create composite key for event", nil)
        }

        eventBytes, err := ObjecttoJSON(event)
        if err != nil {
            return BuildResponse("ERROR", "Failed to marshal updated event data", nil)
        }

        err = ctx.GetStub().PutState(eventKey, eventBytes)
        if err != nil {
            return BuildResponse("ERROR", "Failed to update event in the blockchain", nil)
        }
    }

    return BuildResponse("SUCCESS", "Product and associated events have been updated successfully", nil)
}

*/


// ProductExists checks if a product exists in the world state
func (s *SmartContract) ProductExists(ctx contractapi.TransactionContextInterface, id string) (bool, error) {
	productJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return false, fmt.Errorf("failed to read from world state: %v", err)
	}

	return productJSON != nil, nil
}


