#!/bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#

# This is a collection of bash functions used by different scripts

# imports
. scripts/utils.sh

export CORE_PEER_TLS_ENABLED=true
export ORDERER1_CA=${PWD}/organizations/ordererOrganizations/itblanket.org/orderer1/tlsca/tlsca-cert.pem
export PEER0_FARMER_CA=${PWD}/organizations/peerOrganizations/farmer/tlsca/tlsca.farmer-cert.pem
export PEER0_ORGANICCERTAUTH_CA=${PWD}/organizations/peerOrganizations/organiccertauth/tlsca/tlsca.organiccertauth-cert.pem

# Set environment variables for the peer org
setGlobals() {
  PEER=$1
  local USING_ORG=""
  if [ -z "$OVERRIDE_ORG" ]; then
    USING_ORG=$2
  else
    USING_ORG="${OVERRIDE_ORG}"
  fi
  infoln "Using organization ${USING_ORG}"
  if [ "$USING_ORG" == "farmer" ]; then
    export CORE_PEER_LOCALMSPID="FarmerMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_FARMER_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/farmer/users/Admin@farmer/msp
    if [ $PEER -eq 0 ]; then
      export CORE_PEER_ADDRESS=localhost:4444
    elif [ $PEER -eq 1 ]; then
      export CORE_PEER_ADDRESS=localhost:4454
    fi
  elif [ "$USING_ORG" == "organiccertauth" ]; then
    export CORE_PEER_LOCALMSPID="OrganiccertauthMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_ORGANICCERTAUTH_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/organiccertauth/users/Admin@organiccertauth/msp
    if [ $PEER -eq 0 ]; then
      export CORE_PEER_ADDRESS=localhost:5555
    elif [ $PEER -eq 1 ]; then
      export CORE_PEER_ADDRESS=localhost:5565
    fi

  else
    errorln "ORG Unknown"
  fi

  if [ "$VERBOSE" == "true" ]; then
    env | grep CORE
  fi
}

# Set environment variables for use in the CLI container 
setGlobalsCLI() {
  setGlobals 0 $1

  local USING_ORG=""
  if [ -z "$OVERRIDE_ORG" ]; then
    USING_ORG=$1
  else
    USING_ORG="${OVERRIDE_ORG}"
  fi
  if [ "$USING_ORG" == "farmer" ]; then
    export CORE_PEER_ADDRESS=peer0.farmer:4444
  elif [ "$USING_ORG" == "organiccertauth" ]; then
    export CORE_PEER_ADDRESS=peer0.organiccertauth:5555

  else
    errorln "ORG Unknown"
  fi
}

# parsePeerConnectionParameters $@
# Helper function that sets the peer connection parameters for a chaincode
# operation
parsePeerConnectionParameters() {
  PEER_CONN_PARMS=()
  PEERS=""

  # Loop through the input parameters as an array of strings
  for PARAM in "$@"; do
    setGlobals 0 "$PARAM"
    PEER="peer0.$PARAM"
    ## Set peer addresses
    if [ -z "$PEERS" ]; then
      PEERS="$PEER"
    else
      PEERS="$PEERS $PEER"
    fi

    PEER_CONN_PARMS=("${PEER_CONN_PARMS[@]}" --peerAddresses $CORE_PEER_ADDRESS)

    ## Set path to TLS certificate
    CA="PEER0_${PARAM^^}"_CA
    TLSINFO=(--tlsRootCertFiles "${!CA}")
    PEER_CONN_PARMS=("${PEER_CONN_PARMS[@]}" "${TLSINFO[@]}")
  done

  # Remove leading space for output
  PEERS="$(echo -e "$PEERS" | sed -e 's/^[[:space:]]*//')"
}

verifyResult() {
  if [ $1 -ne 0 ]; then
    fatalln "$2"
  fi
}

