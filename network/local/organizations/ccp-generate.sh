#!/bin/bash

function one_line_pem {
    echo "`awk 'NF {sub(/\\n/, ""); printf "%s\\\\\\\n",$0;}' $1`"
}

function json_ccp {
    local PP=$(one_line_pem $6)
    local CP=$(one_line_pem $7)
    sed -e "s/\${ORG}/$1/" \
        -e "s/\${PEER}/$2/" \
        -e "s/\${P0PORT}/$3/" \
        -e "s/\${P1PORT}/$4/" \
        -e "s/\${CAPORT}/$5/" \
        -e "s#\${PEERPEM}#$PP#" \
        -e "s#\${CAPEM}#$CP#" \
        organizations/ccp-template.json
}

function yaml_ccp {
    local PP=$(one_line_pem $6)
    local CP=$(one_line_pem $7)
    sed -e "s/\${ORG}/$1/" \
        -e "s/\${PEER}/$2/" \
        -e "s/\${P0PORT}/$3/" \
        -e "s/\${P1PORT}/$4/" \
        -e "s/\${CAPORT}/$5/" \
        -e "s#\${PEERPEM}#$PP#" \
        -e "s#\${CAPEM}#$CP#" \
        organizations/ccp-template.yaml | sed -e $'s/\\\\n/\\\n          /g'
}

## prepare connection profile for orgfarmer
ORG=Farmer
PEER=farmer
P0PORT=4444
P1PORT=4454
CAPORT=4400
PEERPEM=organizations/peerOrganizations/farmer/tlsca/tlsca.farmer-cert.pem
CAPEM=organizations/peerOrganizations/farmer/ca/ca.farmer-cert.pem

echo "$(json_ccp $ORG $PEER $P0PORT $P1PORT $CAPORT $PEERPEM $CAPEM)" > organizations/peerOrganizations/farmer/connection-farmer.json
echo "$(yaml_ccp $ORG $PEER $P0PORT $P1PORT $CAPORT $PEERPEM $CAPEM)" > organizations/peerOrganizations/farmer/connection-farmer.yaml
# save another copy of json connection profile in a different directory
echo "$(json_ccp $ORG $PEER $P0PORT $P1PORT $CAPORT $PEERPEM $CAPEM)" > network-config/network-config-farmer.json

## prepare connection profile for orgorganiccertauth
ORG=Organiccertauth
PEER=organiccertauth
P0PORT=5555
P1PORT=5565
CAPORT=5500
PEERPEM=organizations/peerOrganizations/organiccertauth/tlsca/tlsca.organiccertauth-cert.pem
CAPEM=organizations/peerOrganizations/organiccertauth/ca/ca.organiccertauth-cert.pem

echo "$(json_ccp $ORG $PEER $P0PORT $P1PORT $CAPORT $PEERPEM $CAPEM)" > organizations/peerOrganizations/organiccertauth/connection-organiccertauth.json
echo "$(yaml_ccp $ORG $PEER $P0PORT $P1PORT $CAPORT $PEERPEM $CAPEM)" > organizations/peerOrganizations/organiccertauth/connection-organiccertauth.yaml
# save another copy of json connection profile in a different directory
echo "$(json_ccp $ORG $PEER $P0PORT $P1PORT $CAPORT $PEERPEM $CAPEM)" > network-config/network-config-organiccertauth.json




